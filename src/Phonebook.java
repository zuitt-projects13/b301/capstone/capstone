import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook() {}

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

}
