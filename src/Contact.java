import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> contactNumber = new ArrayList<>();
    private ArrayList<String> address = new ArrayList<>();

    public Contact() {}

    public Contact (String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber.add(contactNumber);
        this.address.add(address);
    }

    public Contact (String name, ArrayList<String> contactNumbers, ArrayList<String> addresses) {
        this.name = name;
        this.contactNumber = contactNumbers;
        this.address = addresses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumber;
    }

    public void setContactNumber(ArrayList<String> contactNumbers) {
        this.contactNumber = contactNumbers;
    }

    public void addContactNumber(String contactNumber) {
        this.contactNumber.add(contactNumber);
    }

    public  ArrayList<String>  getAddresses() {
        return address;
    }

    public void setAddress(ArrayList<String> address) {
        this.address = address;
    }

    public void addAddress(String address) {
        this.address.add(address);
    }
}
