
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        // Instantiate Contacts
        Contact contact1 = new Contact("John Doe","+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");

        // Additional Number and address
        contact1.addContactNumber("123");
        contact1.addAddress("ABC Street");
        contact2.addContactNumber("456");
        contact2.addAddress("XYZ Street");

        // Add contacts to phonebook
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        // Displaying content of phonebook
        if (phonebook.getContacts().size() >0) {
            // Display contact details
            for( Contact contact : phonebook.getContacts()) {

                System.out.println(contact.getName());
                System.out.println("--------------------");

                System.out.println(contact.getName() + " has the following registered number:");
                for (String number : contact.getContactNumbers()) {
                    System.out.println(number);
                }

                System.out.println(contact.getName() + " has the following registered address:");
                for (String address : contact.getAddresses()) {
                    System.out.println(address);
                }

                // prints blank line
                System.out.println('\n');

            }
        } else {
            System.out.println("Phonebook is empty.");
        }
    }
}